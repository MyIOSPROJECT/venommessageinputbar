# VenomMessageInputBar

message input bar that support sending photo, capture photo, send message , send audio message 


[Demo video](https://youtu.be/D1u9R60L1oE)

# Usage

   ```
lazy var messageInputBar: VenomMessageInput = {
        let m = VenomMessageInput()
        m.holderController = self
        m.delegate         = self
        return m
    }()

override var inputAccessoryView: UIView? { messageInputBar }

override var canBecomeFirstResponder: Bool { true }

```

